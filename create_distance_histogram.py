#!/usr/bin/python3

import numpy as np
import pdb
import sys

def main(input_file_name):
    overall_histogram = np.zeros(50,dtype=int)
    overall_bins = list(range(51))
    max_distance = 0.0
    with open(input_file_name) as f:
#        pdb.set_trace()
        for i in range(22570):
            if i%1000 == 0:
                sys.stderr.write("{}\n".format(i))
            array = np.fromfile(f, dtype=np.dtype('float'), count=22570)
            max_distance = max(max_distance, array.max())
            histogram, bins = np.histogram(array, bins=overall_bins)
            overall_histogram = overall_histogram + histogram
    for i in range(overall_histogram.shape[0]):
        print("{0:<2}:  {1}".format(i, overall_histogram[i]))
    print("Max distance: {0}".format(max_distance))
    print("Histogram entries: {0}".format(np.sum(overall_histogram)))
    return

if __name__=='__main__':
    main(sys.argv[1])
    
