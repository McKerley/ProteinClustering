#!/usr/bin/python3

import igraph as ig
import numpy as np
import json
import pdb
import sys

def main(report_file_name):

    #pdb.set_trace()
    community_avg = {}
    community_count = {}
    community_min = {}
    community_max = {}
    with open(report_file_name) as report_file:
        report = json.loads(report_file.read())
        with open(report["community_file"]) as community_file:
            communities = np.loadtxt(community_file, delimiter="\t", dtype=int)
                
            if not report["graph_data"].endswith('.gml'):
                raise Exception("Graph file name does not end with .gml: {}".format(report["graph_data"]))
            graph = ig.Graph.Read_GML(report["graph_data"])
            graph.vs["community"] = communities[:,1]
            for community in np.unique(communities[:,1]):
                energies = np.array(graph.vs.select(community_eq=community)["energy"])
                community_count[community] = len(energies)
                community_avg[community] = np.average(energies)
                community_min[community] = np.min(energies)
                community_max[community] = np.max(energies)
    report_format = "{0:<3} {1:<10} {2:<10} {3:<10} {4:10}"
    report_format = "{0},{1},{2},{3},{4}"
    print(report_format.format(*("Community Count Avg Min Max".split())))
    for community in sorted(community_avg.keys()):
        print(report_format.format(
            community,
            community_count[community],
            community_avg[community],
            community_min[community],
            community_max[community]))
    return

if __name__=='__main__':
    main(sys.argv[1])
    
