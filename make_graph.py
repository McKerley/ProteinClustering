#!/usr/bin/python3

import argparse
import igraph as ig
import numpy as np
import pdb
import pprint
import scipy.spatial
import sys
import CommunityDetection

def read_data(input_file_name, verbose=False):
    if verbose:
        sys.stderr.write("Reading data\n")
    min_distance = np.inf
    max_distance = -np.inf

    points = []
    energy = []
    with open(input_file_name) as f:
        line_counter = 0
        for line in f:
            if verbose and line_counter%1000 == 0:
                sys.stderr.write("{}\r".format(line_counter))
            line_counter += 1
            fields = line.strip().split(" ")
            if len(fields) != 15:
                raise Exception("File not properly formatted: line {} has {} fields, not 15".format(line_count, len(fields)))
            points.append(fields[5:])
            energy.append(fields[3])

    if verbose:
        sys.stderr.write("\n")

    points = np.array(points, dtype=np.float64)
    energy = np.array(energy, dtype=np.float64)

    return points, energy

def find_edges(points, neighborhood_radius, max_neighbors, verbose=False):
    if verbose:
        sys.stderr.write("Finding edges for k = {}\n".format(neighborhood_radius))
    counter = 0
    num_points = points.shape[0]
    edges = []

    for i in range(num_points):
        if verbose and i%1000 == 0:
            sys.stderr.write("{}\r".format(i))

        # find distances to all other points for this point
        distances = scipy.spatial.distance.cdist([points[i]], points, metric='euclidean')[0]

        # get the indexes of the valid neighbors (ie, distance < neighborhood_radius)
        neighbors = (np.where(distances<=neighborhood_radius))[0]

        # Get these distances for sorting
        neighbor_distances = distances[neighbors]
        
        # Get the indexes of sorting the neighbor distances
        sorted_indexes = np.argsort(neighbor_distances)[:max_neighbors+1]

        # Get indexes from original neighbors list for edges
        edges.extend([(i,j) for j in neighbors[sorted_indexes] if i != j])
        if verbose:
            if np.any(distances[neighbors[sorted_indexes]] > neighborhood_radius):
                raise Exception("Added edge(s) with distance > max")
            
    if verbose:
        sys.stderr.write("\n")
        
    return edges

def main(input_file_name, output_file_name, neighborhood_radius, max_neighbors, verbose=False, debug=False):
    points, energy = read_data(input_file_name, verbose=verbose)
    if debug:
        pdb.set_trace()
    edges = find_edges(points, neighborhood_radius, max_neighbors, verbose=verbose)
    graph = ig.Graph(points.shape[0])
    graph.add_edges(edges)
    graph.vs["energy"] = energy

    if verbose:
        sys.stderr.write(pprint.pformat(graph.summary()))
        sys.stderr.write("Writing graph to {}\n".format(output_file_name))
    with open(output_file_name, "w") as output:
        graph.write_gml(output)

    return

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-v',
                        action='store_true',
                        help='Verbose mode',
                        default=False)
    parser.add_argument('-d',
                        action='store_true',
                        help='Debug mode',
                        default=False)
    parser.add_argument('-e',
                        type=float,
                        help='Nearest-neighborhood radius.',
                        default=2.0)
    parser.add_argument('-k',
                        type=int,
                        help='Max nearest neigbors.',
                        default=2.0)
    parser.add_argument('-i',
                        type=str,
                        default='',
                        help='Input file.')
    parser.add_argument('-o',
                        type=str,
                        default='',
                        help='Output gml file.')

    args = parser.parse_args()
    main(args.i, args.o, args.e, args.k, verbose=args.v, debug=args.d)
    
