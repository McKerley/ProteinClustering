#!/usr/bin/python3

import numpy as np
import pdb
import scipy
import scipy.spatial
import sys

def main(input_file_name, output_file_name):
    pdb.set_trace()
    min_distance = np.inf
    max_distance = -np.inf
    with open(output_file_name, 'w') as of:
        with open(input_file_name) as f:
            line_counter = 0
            data = []
            for line in f:
                line_counter += 1
                fields = line.strip().split(" ")
                if len(fields) != 15:
                    raise ValueException("File not properly formatted: line {} has {} fields, not 15".format(line_count, len(fields)))
                data.append(list(map(float, fields[5:])))

        data = np.array(data)
        counter = 0
        for p in data:
            counter += 1
            if counter%100 == 0:
                print(counter)
            distances = scipy.spatial.distance.cdist(np.array([p]), data, metric='euclidean')
            distances[0].tofile(of)
            max_distance = max(max_distance, distances.max())
            min_distance = min(min_distance, distances.min())

        print("max={}, min={}".format(max_distance, min_distance))

    return

if __name__=='__main__':
    main(sys.argv[1],sys.argv[2])
    
